
resume.pdf: resume.tex
	xelatex $<

.PHONY: clean

clean:
	$(RM) -f resume.pdf resume.out resume.log resume.aux
